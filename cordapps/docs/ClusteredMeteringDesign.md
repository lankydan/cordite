<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Clustered Metering

## Overview

Metering on clustered notaries presents a few technical challenges. At the moment, Corda provides a RAFT cluster and a Smart BFT Cluster. At this point in time it is likely that the implementation of the BFT cluster will change. In this document we aim to produce an approach that will work for all types of clusters, be they completely replicated (RAFT) or replicated with partial consensus (BFT).

The key challenges are:
1. Ensuring only one of the Notaries in the cluster issues the metering invoice.
2. Ensuring that the metering services are not affected by notaries leaving or joining the cluster.
3. Ensuring that metering invoices raised by malicious notaries are rejected.
4. Ensuring that dispute resolution and dispersals function correctly.
5. Security of notary services is not compromised by metering.

The following sections give a brief overview of how the current clusters work and the proposed approach to enable metering to function on these clusters.

## Brief description of the RAFT Notary.

The RAFT notary functionality is primarily handled by the RaftUniquenessProvider class.
This class consumes the Raft functionality provided by the atomix library (https://atomix.io/)
The uniqueness provider acts as a member of the cluster as well as being a client of the cluster.
Notarised transactions are stored in a RaftState class which is stored in a DistributedImmutableMap. This map is replicated and persisted to all nodes.
At the moment the RaftState does not hold the Identity of the creator of the transaction which we need in order to create a metering invoice.
The ‘commit’ function on the uniqueness provider commits new transaction states to the DistributedImmutableMap via the CopyCat Client. The CopyCat client distributes the
transaction across the cluster and checks that the transaction is unique.
As with all specialised notaries, the Raft notary provides the ‘receiver’ end of the notary flow.
One outstanding question is how the customer ‘node’ decides which member of the notary cluster to send the initial transaction to as it uses a shared legal entity - the code does not seem to reveal this easily (Note to self : debug this when you get a notary cluster set up)

## Brief Description of the BFT Smart Notary

TBA

## Approach 1 for metering RAFT notary - 'Leader/Meterer'

The cordite metering service runs on all nodes in the cluster

The Raft Notary is amended to provide a callback hook which the the metering service can listen to for new transactions.
If it is the leader, it takes the new transaction and puts this on a ‘metering’ distributed map with the metering state as UN_METERED,

As it is a unique map, only non duplicate metering invoices will be added.

We create a RaftMeterer derived from AbstractMeterer that periodically checks the Metering Map for UN_METERED transactions.
If we are the RAFT leader, then a metering invoice is created and sent to the original client - this will only go into the vault of the Leader (see potential problems below)

When the flow starts we should mark the Metering invoice in the distributed map as 'ABOUT_TO_ISSUE' - this will act as a softlock to stop it being selected when the cluster leadership changes.

If the flow completes we update the metering invoice state in the Distributed map as ‘ISSUED'
if it fails, then we log out the error and move to the next metering invoice.

If the node goes offine halfway through the flow, can we make the assumption that the flow will recover and complete - this does mean that we should update the distributed map in the flow (after wait for ledger commit).

### What we need for approach 1:

1. ‘Raft’ specific implementation of the AbstractMeterer with hooks into the RaftUniqueness provider - we may have to make the MeteringService a fully fledged ‘Notary’ Service - i.e. derived from it.
2. New ‘Metering’ distributed map to hold the new ‘Unmetered’ metering invoices.
3. ‘Listener hook’ into RaftNotary service so that we can listen for new metering invoices or 'commit response' hook from the client ‘commit’ function, so that we can add the new metering invoices when all cluster nodes have responded.
4. Metering invoice is changed so that it can hold an array of metering node identies (so that they all get paid when the invoice is dispersed)
5. Depending on the 2) above, we may need to add the identity of the node that created the transaction so that we can create the metering invoice.
6. A way of detecting if we are the current Raft Leader - be it via poll or event callback.

### Pros/Cons with approach 1:

#### Pros

1. Works in sync with the Raft Cluster and uses it's functionality - mitigating the need for some other form of metering 'leadership' protocol.
2. Metering can run on any node and can recover from node failure.
3. Minimum changes to Corda Core Code - we only need to add a couple of hooks.
3. Participants in the cluster are rewarded per notarised transaction so it gives them more incentive to be up and running.

#### Cons
1. If a node goes offline forever, then we could potentially be left with a metering invoice stuck in a flow. This could be detected by checking for the 'ABOUT_TO_ISSUE' state and how long that state has remained. And then it could be re-issued after a period of time. If the failed node recovers, then the flow will fail with a duplicate.
2. Metering invoices will only be created on the leader node - meaning that disputing metering invoices will be tricky - i.e. how do we pick the right node? - can a flow be aimed at the cluster? - in theory yes as this could mirror the RaftNotaryFlow. But when which node does the disputed metering invoice GUI go to - in theory this could look at the distributed map. It would have to be the policy of the metering cluster for any node to handle disputes.

## Approach 2 for RAFT Notary - Special node

We create a special ‘Metering Node’ which joins the RAFT Cluster - all this does is listen for ‘committed’ transactions - it does not have to be the leader
It then runs like a standard single node metered - for prod, this could run in HA mode.
This is relatively easy to implement in that we just have a metering service config, with the X500 name of the target node.

### What we need for Approach 2

Same as approach 1, with the exception point 6 as we don't need to be a leader of the cluster to notarise transaction.

### Pros/Cons with Approach 2

### Pros

1. It is simpler to manage disputes as this is a single node.

### Cons

1. This relies on single node for metering which sort of defeats the purpose of having a
Raft Cluster. But as a stepping stone to full availability this might be an option as managing metering disputes could prove to be complex.

## Other Questions / observations on RAFT

When looking at the states of the notaries in the RAFT demo - we need to understand why not all of the states appear on all the nodes - seems like it is not working, but - need to look again. 

## Approach for BFT or other non fully replicated clusters.  

Here are some theoretical thoughts on this as I’ve not looked too deeply at it and also it is changing at the moment, so theory will do for now.

The main issue for metering on BFT is knowing if you are a faulty replica or correct one.
  
You could make the assumption that as the ‘end’ of notary flow that the request is valid.
Unless the malicious notary was colluding with the client, in which case the replicas would reject the message (in theory)
If the malicious notary randomly created a transaction state and sent it to a client - it would be rejected as the client would not find the transaction in it’s vault (the metering flows deal with this).

Conclusion is then that the metering invoice can be created when the replicas come back with enough replies

Like Raft - we have two choices:
1) run metering on a single node and listen to events - use that node as the single dispute node.
2) run metering on all nodes where the metering state is persisted and ‘disputed’ on that notary this would work in that the node owner is the producer of the metering invoice.

We could also consider in both cases how we would propagate the state to all nodes for subsequent disputes - this could be handled as part of the Issue Metering invoice flow.

Questions:

Is there a way of telling if you are a faulty replica? - or does this even matter?

## Distributed metering utopia on notary clusters.

If we had no restriction over specific technology implementations this is how 

Ideally metering invoices would be distributed to all nodes.
If these metering invoices were disputed, then any of the nodes could respond - in theory we can do this by making each present ‘notary’ members of the metering invoice (we can do this in theory with BFT for those that correctly return the notary transactions) - i.e stamp it with all the notaty nodes that were present when the invoice was raised - if a notary drops out the pool it does not matter.

i.e. you could do this 
Notary (flow) receives transaction
Transaction is sent to cluster
wait for enough Qurom in the respondants
gather the public keys/identieis of the respondents
create a metering invoice - add all respondants + target - could the owner be a composite key?
if failed on one respond - remove and try again
business node can now dispute to any of the participants.

How do dispersals work
	All participants (other than the client) are paid a percentage split of the fee.
	This makes it even more fun if the guardian notaries are also a pool - the split would have to go to each member of that pool as well (same pattern as the original invoice)

	So this means that all the ‘well behaving nodes’ must be on the participant list.





