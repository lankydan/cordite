/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils.jackson

import com.fasterxml.jackson.databind.module.SimpleModule
import io.bluebank.braid.corda.serialisation.BraidCordaJacksonInit
import io.vertx.core.json.Json
import net.corda.core.node.services.vault.PageSpecification
import java.time.Instant

class CorditeJacksonInit {
  companion object {
    init {
      BraidCordaJacksonInit.init()
      // TODO: move the following to `braid-corda` module
      val sm = SimpleModule()
          .addSerializer(PageSpecification::class.java, PageSpecificationSerializer())
          .addDeserializer(PageSpecification::class.java, PageSpecificationDeserializer())
          .addDeserializer(Instant::class.java, InstantDeserializer())
      Json.mapper.registerModule(sm)
      Json.prettyMapper.registerModule(sm)
    }
    fun init() {
      // automatically runs the class init once only
    }
  }

}