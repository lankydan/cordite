/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl

import io.bluebank.braid.client.BraidProxyClient
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.account.CreateAccountFlow
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.tag.WellKnownTagValues
import io.cordite.dgl.corda.token.TokenType
import io.cordite.test.utils.h2.H2Server
import io.cordite.test.utils.BraidClientHelper
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.run
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.contracts.Amount
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.utilities.getOrThrow
import net.corda.finance.contracts.asset.OnLedgerAsset
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestNode(val node: StartedMockNode, braidPortHelper: BraidPortHelper) {
  private val braidClient: BraidProxyClient

  val party: Party = node.info.legalIdentities.first()
  val ledgerService: LedgerApi

  init {
    braidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "ledger")
    ledgerService = braidClient.bind(LedgerApi::class.java)
  }

  fun shutdown() {
    braidClient.close()
  }
}


@RunWith(VertxUnitRunner::class)
class DglFlowTests {
  companion object {
    lateinit var network: MockNetwork
    private lateinit var ledgerNodeA: LedgerApi
    private lateinit var ledgerNodeB: LedgerApi
    private val nodeNameA = CordaX500Name("nodeA", "London", "GB")
    private val nodeNameB = CordaX500Name("nodeB", "London", "GB")
    private lateinit var h2Server: H2Server
    private val braidPortHelper = BraidPortHelper()
    private lateinit var testNodeA: TestNode
    private lateinit var testNodeB: TestNode
    private val intFountain = IntFountain()
    private lateinit var defaultNotaryName: CordaX500Name
    private lateinit var nodeA : StartedMockNode
    private lateinit var nodeB: StartedMockNode

    @JvmStatic
    @BeforeClass
    fun beforeClass() {
      braidPortHelper.setSystemPropertiesFor(nodeNameA, nodeNameB)
      network = MockNetwork(listOf(this::class.java.`package`.name, OnLedgerAsset::class.java.`package`.name))
      nodeA = network.createPartyNode(nodeNameA)
      nodeB = network.createPartyNode(nodeNameB)
      network.runNetwork()
      testNodeA = TestNode(nodeA, braidPortHelper)
      testNodeB = TestNode(nodeB, braidPortHelper)
      ledgerNodeA = testNodeA.ledgerService
      ledgerNodeB = testNodeB.ledgerService
      h2Server = H2Server(network, listOf(nodeA, nodeB))
      defaultNotaryName = network.defaultNotaryIdentity.name
    }

    @JvmStatic
    @AfterClass
    fun afterClass() {
      testNodeA.shutdown()
      testNodeB.shutdown()
      h2Server.stop()
      network.stopNodes()
    }
  }

  private val xkcdSymbol = "XKCD${intFountain.next()}"
  private val corditeSymbl = "XCD${intFountain.next()}"
  private val accountId1 = "account-${intFountain.next()}"
  private val accountId2 = "account-${intFountain.next()}"
  private val accountId3 = "account-${intFountain.next()}"
  private val accountId4 = "account-${intFountain.next()}"
  private val swiftCodeTag = Tag(WellKnownTagCategories.SWIFT_CODE, "AACCGB21" + intFountain.next())

  private val cashAccountLabel = "CASH_ACCOUNT"
  private val britishAccounts = "BRIT_ACCOUNTS"
  private val europeanAccounts = "EURO_ACCOUNTS"
  private val allAccounts = "ALL_ACCOUNTS"

  private val allAccountsTag = Tag(cashAccountLabel, allAccounts)
  private val britishAccountsTag = Tag(cashAccountLabel, britishAccounts)
  private val europeanAccountsTag = Tag(cashAccountLabel, europeanAccounts)


  @Test(timeout = Long.MAX_VALUE)
  fun `create account and check we can find it using its tag`() {
    run(network) { ledgerNodeA.createAccount(accountId1, defaultNotaryName) }
    val accountV2 = run(network) { ledgerNodeA.setAccountTag(accountId1, swiftCodeTag, defaultNotaryName) }
    assertEquals(accountV2, run(network) { ledgerNodeA.findAccountsByTag(swiftCodeTag) }.first())
    run(network) { ledgerNodeA.removeAccountTag(accountId1, swiftCodeTag.category, defaultNotaryName) }
    assertEquals(0, run(network) { ledgerNodeA.findAccountsByTag(swiftCodeTag) }.count())
  }

  @Test(timeout = Long.MAX_VALUE)
  fun `create token and check that it exists`() {
    val token = run(network) { ledgerNodeA.createTokenType(xkcdSymbol, 2, defaultNotaryName) }
    val tokens = run(network) { ledgerNodeA.listTokenTypes() }
    assertTrue(tokens.contains(token))
  }

  @Test(timeout = Long.MAX_VALUE)
  fun `issue tokens`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    val corditeTokenType = ledgerNodeA.createTokenType(corditeSymbl)
    ledgerNodeA.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next

    run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }

    // issue a bunch more tokens
    run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }

    run(network) { ledgerNodeA.issueToken(accountId1, amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId1, amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }

    val balances1 = run(network) { ledgerNodeA.balanceForAccount(accountId1) }.map { it.token to it }.toMap()
    val balances2 = run(network) { ledgerNodeA.balanceForAccount(accountId2) }.map { it.token to it }.toMap()

    assertEquals(BigDecimal("200.00"), balances1.getValue(corditeTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("200.00"), balances1.getValue(xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("300.00"), balances2.getValue(corditeTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("200.00"), balances2.getValue(xkcdTokenType.descriptor).toDecimal())
  }

  // TODO: this is due to https://gitlab.com/cordite/cordite/issues/194
  @Ignore
  @Test
  fun `that we can get well known tag categories`() {
    val categories = ledgerNodeA.wellKnownTagCategories()
    val values = ledgerNodeA.wellKnownTagValues()

    val categoryCount = WellKnownTagCategories::class.java.fields.filter { it.type == String::class.java }.count()
    val valueCount = WellKnownTagValues::class.java.fields.filter { it.type == String::class.java }.count()
    assertEquals(categoryCount, categories.count())
    assertEquals(valueCount, values.count())
  }

  @Test
  fun `issue tokens and transfer between internal accounts`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val amount = "100.00"
    val fountain = intFountain::next
    run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())

    run(network) {
      ledgerNodeA.transferToken(
        "5.00",
        xkcdTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
        "$accountId2@${testNodeA.node.info.legalIdentities.first().name}",
        "transfer", defaultNotaryName)
    }

    assertEquals(BigDecimal("95.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("5.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())

    run(network) {
      ledgerNodeA.transferToken(
        "5.00",
        xkcdTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
        "$accountId2@${testNodeA.node.info.legalIdentities.first().name}",
        "transfer", defaultNotaryName)
    }

    assertEquals(BigDecimal("90.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("10.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())
  }

  @Test
  fun `issue tokens and transfer between accounts on distinct parties`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())

    run(network) {
      ledgerNodeA.transferToken(
        "5.00",
        xkcdTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
        "$accountId2@${testNodeB.node.info.legalIdentities.first().name}",
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("95.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("5.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())

    run(network) {
      ledgerNodeA.transferToken(
        "5.00",
        xkcdTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
        "$accountId2@${testNodeB.node.info.legalIdentities.first().name}",
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("90.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("10.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal())
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test(timeout = Long.MAX_VALUE)
  fun `that we can query the balances of an account without having created tokens`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal())
  }

  @Test(timeout = Long.MAX_VALUE)
  fun `create accounts and get aggregated balance using its tags`() {
    run(network) { ledgerNodeA.createAccount(accountId1, defaultNotaryName) }
    run(network) { ledgerNodeA.createAccount(accountId2, defaultNotaryName) }
    run(network) { ledgerNodeA.createAccount(accountId3, defaultNotaryName) }
    run(network) { ledgerNodeA.createAccount(accountId4, defaultNotaryName) }

    run(network) { ledgerNodeA.setAccountTag(accountId1, allAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId2, allAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId3, allAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId4, allAccountsTag, defaultNotaryName) }

    run(network) { ledgerNodeA.setAccountTag(accountId1, britishAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId2, britishAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId3, europeanAccountsTag, defaultNotaryName) }
    run(network) { ledgerNodeA.setAccountTag(accountId4, europeanAccountsTag, defaultNotaryName) }

    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)

    val fountain = intFountain::next
    run(network) { ledgerNodeA.issueToken(accountId1, "25.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId2, "50.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId3, "75.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    run(network) { ledgerNodeA.issueToken(accountId3, "100.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }

    assertEquals(BigDecimal("250.00"), ledgerNodeA.balanceForAccountTag(allAccountsTag, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccountTag(britishAccountsTag, xkcdTokenType.descriptor).toDecimal())
    assertEquals(BigDecimal("175.00"), ledgerNodeA.balanceForAccountTag(europeanAccountsTag, xkcdTokenType.descriptor).toDecimal())
  }

  @Test
  fun `collect coins when sufficient balance available`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val smallChange = 1
    val fountain = intFountain::next
    val coins = 100
    (1..coins).forEach {
      run(network) { ledgerNodeA.issueToken(accountId1, smallChange.toBigDecimal().toString(), xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    }
    val balance1 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal()
    assertTrue(smallChange.toBigDecimal().multiply(coins).compareTo(balance1) == 0)

    val transfer = smallChange.toBigDecimal().multiply(coins - 1).minus(smallChange.toBigDecimal().divide(2))
    val expectedRemainder = balance1.minus(transfer)

    run(network) {
     ledgerNodeA.transferToken(transfer.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
    }
    val balance2 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal()
    assertTrue(expectedRemainder.compareTo(balance2) == 0)

    val balance3 = ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal()
    assertTrue(transfer.compareTo(balance3) == 0)
  }

  @Test
  fun `collect coins when insufficient balance available should fail but still allow future requests that are within the balance`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val smallChange = 1
    val fountain = intFountain::next
    val coins = 100
    (1..coins).forEach {
      run(network) { ledgerNodeA.issueToken(accountId1, smallChange.toBigDecimal().toString(), xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    }
    val balance1 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal()
    assertTrue(smallChange.toBigDecimal().multiply(coins).compareTo(balance1) == 0)

    val transfer1 = smallChange.toBigDecimal().multiply(coins).plus(smallChange.toBigDecimal().divide(2))

    try {
      run(network) {
        ledgerNodeA.transferToken(transfer1.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
      }
      throw IllegalStateException("transfer should have raised an exception because of insufficient funds")
    }
    catch (err: IllegalStateException) {
      throw err
    }
    catch (err: Throwable) {
      println("expected exception caught")
      println(err)
    }

    val transfer2 = smallChange.toBigDecimal().multiply(coins - 1).minus(smallChange.toBigDecimal().divide(2))
    val expectedRemainder = balance1.minus(transfer2)

    run(network) {
      ledgerNodeA.transferToken(transfer2.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
    }
    val balance2 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType.descriptor).toDecimal()
    assertTrue(expectedRemainder.compareTo(balance2) == 0)

    val balance3 = ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType.descriptor).toDecimal()
    assertTrue(transfer2.compareTo(balance3) == 0)
  }

  @Test(expected = CreateAccountFlow.Exception::class)
  fun `that calling CreateAccountFlow with zero requests raises an appropriate exception`() {
    val future = nodeA.startFlow(CreateAccountFlow(listOf(), network.defaultNotaryIdentity))
    network.runNetwork()
    future.getOrThrow()
  }

  @Test
  fun `that transferring to an unknown remote account fails`() {
    try {
      val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
      ledgerNodeA.createAccounts()
      val amount = "100.00"
      val fountain = intFountain::next
      run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
      run(network) {
        ledgerNodeA.transferToken(
          "5.00",
          xkcdTokenType.descriptor.uri,
          "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
          "unknown-account@${testNodeB.node.info.legalIdentities.first().name}",
          "transfer", defaultNotaryName)
      }
      throw Exception("transfer should have failed")
    } catch (ex: RuntimeException) {
      // all good
    }
  }

  @Test
  fun `that transferring to an unknown local account fails`() {
    try {
      val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
      ledgerNodeA.createAccounts()
      val amount = "100.00"
      val fountain = intFountain::next
      run(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
      run(network) {
        ledgerNodeA.transferToken(
          "5.00",
          xkcdTokenType.descriptor.uri,
          "${WellKnownTagCategories.DGL_ID}:$accountId1@${testNodeA.node.info.legalIdentities.first().name}",
          "unknown-account@${testNodeA.node.info.legalIdentities.first().name}",
          "transfer", defaultNotaryName)
      }
      throw Exception("transfer should have failed")
    } catch (ex: RuntimeException) {
      // all good
    }
  }

  private fun LedgerApi.createAccounts() {
    run(network) { createAccount(accountId1, defaultNotaryName) }
    setAccountTag(accountId1, swiftCodeTag, defaultNotaryName)
    run(network) { createAccount(accountId2, defaultNotaryName) }
  }

  private fun LedgerApi.createTokenType(symbol: String): TokenType.State {
    return run(network) { this.createTokenType(symbol, 2, defaultNotaryName) }
  }

  private fun LedgerApi.balanceForAccount(account: String, tokenType: TokenType.Descriptor): Amount<TokenType.Descriptor> {
    val balances = run(network) { balanceForAccount(account) }.map { it.token to it }.toMap()
    return if (balances.containsKey(tokenType)) {
      balances.getValue(tokenType)
    } else {
      Amount(0, tokenType)
    }
  }

  private fun LedgerApi.balanceForAccountTag(tag: Tag, tokenType: TokenType.Descriptor): Amount<TokenType.Descriptor> {
    val balances = run(network) { balanceForAccountTag(tag) }.map { it.token to it }.toMap()
    return if (balances.containsKey(tokenType)) {
      balances.getValue(tokenType)
    } else {
      Amount(0, tokenType)
    }
  }
}

class IntFountain {
  private val atomicInt = AtomicInteger(1)
  fun next() = atomicInt.getAndIncrement()
}

fun Int.toBigDecimal() : BigDecimal {
  return BigDecimal(this.toLong())
}

fun Double.toBigDecimal(): BigDecimal {
  return BigDecimal(this)
}

fun BigDecimal.multiply(value: Int) : BigDecimal {
  return this.multiply(value.toBigDecimal())
}

fun BigDecimal.minus(value: Int) : BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.minus(value: Double) : BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.divide(value: Int) : BigDecimal {
  return this.divide(value.toBigDecimal())
}