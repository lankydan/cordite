/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda

import io.bluebank.braid.core.annotation.MethodDescription
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.token.TokenTransactionSummary
import io.cordite.dgl.corda.token.TokenType
import io.cordite.scheduler.ScheduledOperation
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import net.corda.core.contracts.Amount
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.services.vault.DEFAULT_PAGE_NUM
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.core.node.services.vault.PageSpecification
import rx.Observable
import java.time.LocalDateTime

// a more succinct API for testing the ledger
interface LedgerApi {
  fun wellKnownTagCategories() : List<String>

  fun wellKnownTagValues() : List<String>

  fun createTokenType(
    symbol: String,
    exponent: Int,
    notary: CordaX500Name
  ) : Future<TokenType.State>


  fun listTokenTypes(
      page: Int = DEFAULT_PAGE_NUM,
      pageSize: Int = DEFAULT_PAGE_SIZE
  ): Future<List<TokenType.State>>

  fun listTokenTypes() = listTokenTypes(DEFAULT_PAGE_NUM, DEFAULT_PAGE_SIZE)

  @MethodDescription(description = "create an account", returnType = Account.State::class)
  fun createAccount(accountId: String, notary : CordaX500Name) : Future<Account.State>

  @MethodDescription(description = "create a set of accounts", returnType = Account.State::class)
  fun createAccounts(requests: List<CreateAccountRequest>, notary: CordaX500Name) : Future<Set<Account.State>>

  fun setAccountTag(accountId: String, tag: Tag, notary : CordaX500Name) : Future<Account.State>

  fun removeAccountTag(accountId: String, category: String, notary: CordaX500Name) : Future<Account.State>

  fun getAccount(accountId: String): Future<Account.State>

  fun findAccountsByTag(tag: Tag): Future<Set<Account.State>>

  fun listAccounts(): Future<List<Account.State>>

  fun listAccountsPaged(paging: PageSpecification): Future<List<Account.State>>

  fun issueToken(
      accountId: String,
      amount: String,
      symbol : String,
      description: String,
      notary: CordaX500Name
  ) : Future<SecureHash>

  fun balanceForAccount(accountId: String): Future<Set<Amount<TokenType.Descriptor>>>

  fun balanceForAccountTag(tag : Tag): Future<Set<Amount<TokenType.Descriptor>>>

  fun transferToken(
      amount: String,
      tokenTypeUri: String,
      fromAccount: String,
      toAccount: String,
      description: String,
      notary: CordaX500Name
  ) : Future<SecureHash>

  fun scheduleEvent(clientId: String, payload: JsonObject, iso8601DateTime: LocalDateTime, notary: CordaX500Name) : Future<SecureHash>
  fun listenForScheduledEvents(clientId: String): Observable<ScheduledOperation.State>

  fun transactionsForAccount(accountId: String, paging: PageSpecification) : List<TokenTransactionSummary.State>
  @MethodDescription(returnType = TokenTransactionSummary.State::class, description = "listen for transactions against one or more accounts")
  fun listenForTransactions(accountIds: List<String>) : Observable<TokenTransactionSummary.State>
  fun listenForTransactionsWithPaging(accountIds: List<String>,paging: PageSpecification) : Observable<TokenTransactionSummary.State>
}

data class CreateAccountRequest(val accountId: String)