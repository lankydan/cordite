/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.flows.TransferTokenSenderFunctions.Companion.collectTokenMoveSignatures
import io.cordite.dgl.corda.token.flows.TransferTokenSenderFunctions.Companion.prepareTokenMoveWithSummary
import net.corda.core.contracts.Amount
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
@StartableByService
class TransferTokenFlow(
    private val fromAccount: AccountAddress,
    private val toAccount: AccountAddress,
    private val amount: Amount<TokenType.Descriptor>,
    private val description: String = "",
    private val notary: Party
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val txb = TransactionBuilder(notary = notary)
    val inputSigningKeys = prepareTokenMoveWithSummary(txb, fromAccount, toAccount, amount, serviceHub, ourIdentity, description)
    val tx = serviceHub.signInitialTransaction(txb, inputSigningKeys)
    val stx = collectTokenMoveSignatures(tx, serviceHub, toAccount)
    val secureHash = subFlow(FinalityFlow(stx)).id
    return waitForLedgerCommit(secureHash)
  }
}
