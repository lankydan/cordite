/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.token.Token
import io.cordite.dgl.corda.token.TokenTransactionSummary
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import io.cordite.dgl.corda.token.flows.TokenTransactionSummaryFunctions.addTokenTransactionSummary
import io.cordite.dgl.corda.token.verifyAccountExists

@InitiatingFlow
@StartableByRPC
@StartableByService
class IssueTokenFlow(private val token: Token.State,
                     private val notary: Party,
                     private val description: String)
  : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    verifyAccountExists(serviceHub, token.accountAddress)
    val txb = TransactionBuilder(notary)
    val stx = serviceHub.signInitialTransaction(txb.apply {
      val command = Token.Command.Issue()
      addCommand(command, ourIdentity.owningKey)
      addOutputState(token, token.contractId)
      addTokenTransactionSummary(command, ourIdentity, description, listOf(), listOf(
          TokenTransactionSummary.NettedAccountAmount(token.accountAddress, token.amount.quantity, token.amount.token.product)
      ))
    })
    val secureHash = subFlow(FinalityFlow(stx)).id
    return waitForLedgerCommit(secureHash)
  }
}