/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.api


import com.fasterxml.jackson.annotation.JsonProperty
import io.cordite.metering.contract.*
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import io.vertx.core.Future
import jdk.nashorn.internal.parser.TokenType
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

interface MeteringApi {

  fun listInvoiceTxIds(meteringState: MeteringState?): List<String>
  fun listInvoices(meteringState: MeteringState? = null): List<MeteringInvoiceDetails>
  fun listInvoiceSplits(meteringSplitState: MeteringSplitState? = null): Future<List<MeteringInvoiceSplitDetails>>
  fun payInvoice(payRequest: MeteringInvoicePayRequest): Future<SimpleResult>
  fun disputeInvoice(disputedInvoice: MeteringInvoice): Future<SimpleResult>
  fun reissueInvoice(invoiceToReissue: MeteringInvoiceReissueRequest ): Future<SimpleResult>

  // multiple operations:
  fun payInvoices( payRequests: MeteringInvoicePayRequests): Future<SimpleResult>
  fun disputeInvoices( disputedInvoices: MeteringInvoices ): Future<SimpleResult>
  fun reissueInvoices( invoicesToReissue: MeteringInvoiceReissueRequests ): Future<SimpleResult>
}

// some parameter data classes for the API
// =======================================

// result object for succeed/fail type operations
@CordaSerializable
data class SimpleResult (@JsonProperty var result: Boolean = false, @JsonProperty var reason: String? ) {}

// parameter class to identify a single metering invoice
@CordaSerializable
data class MeteringInvoice (@JsonProperty var meteredTransactionId: String ) {}

// parameter class to identify several metering invoices
@CordaSerializable
data class MeteringInvoices (@JsonProperty var meteredTransactionIds: List<String> ) {}

// parameter class for a single metering invoice payment request
@CordaSerializable
data class MeteringInvoicePayRequest (@JsonProperty var meteredTransactionId: String, @JsonProperty var fromAccount: String ) {}

// parameter class for a bulk invoice payment request
@CordaSerializable
data class MeteringInvoicePayRequests (@JsonProperty var meteredTransactionIds: List<String>, @JsonProperty var fromAccount: String ) {}

// parameter class for a single metering invoice payment request
@CordaSerializable
data class MeteringInvoiceReissueRequest (@JsonProperty var meteredTransactionId: String, @JsonProperty var tokenType: io.cordite.dgl.corda.token.TokenType.Descriptor, @JsonProperty var amount: Int ) {}

// parameter class for a bulk invoice payment request
@CordaSerializable
data class MeteringInvoiceReissueRequests (@JsonProperty var reissueRequests: List<MeteringInvoiceReissueRequest> ) {}

// wrapper class for metering invoice properties, adding invoice owner
@CordaSerializable
data class MeteringInvoiceDetails (@JsonProperty var meteringInvoiceProperties: MeteringInvoiceProperties?, @JsonProperty var invoiceOwner: String? ) {}

// wrapper class for metering invoice split properties, adding invoice owner
@CordaSerializable
data class MeteringInvoiceSplitDetails (@JsonProperty var meteringInvoiceSplitProperties: MeteringInvoiceSplitProperties?, @JsonProperty var invoiceOwner: String? ) {}
