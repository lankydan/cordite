/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.serialization.SerializationWhitelist

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
@StartableByService
class Initiator : FlowLogic<Unit>() {
  @Suspendable
  override fun call() {
    return Unit
  }
}

@InitiatedBy(Initiator::class)
class Responder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
  @Suspendable
  override fun call() {
    return Unit
  }
}

// Serialization whitelist.
class TemplateSerializationWhitelist : SerializationWhitelist {
  override val whitelist: List<Class<*>> = listOf(TemplateData::class.java)
}

// This class is not annotated with @CordaSerializable, so it must be added to the serialization whitelist, above, if
// we want to send it to other nodes within a flow.
data class TemplateData(val payload: String)
