/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.economics

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.BaseModelData
import io.cordite.dao.core.DaoState
import io.cordite.dao.core.ModelDataEvent
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.NewMemberModelDataEvent
import io.cordite.dao.proposal.ProposalState
import io.cordite.commons.utils.contextLogger
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.account.CreateAccountFlow
import io.cordite.dgl.corda.account.SetAccountTagFlow
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.token.CreateTokenTypeFlow
import io.cordite.dgl.corda.token.TokenType
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

val DAO_TAG = Tag("dao","")

@CordaSerializable
data class EconomicsModelData(val issuableTokens: List<TokenType.Descriptor>) : BaseModelData() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    handleAcceptance(inputDao, flowLogic)
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    handleAcceptance(inputDao, flowLogic)

  }

  @Suspendable
  private fun handleAcceptance(inputDao: DaoState, flowLogic: FlowLogic<*>) {
    log.info("handling acceptance of economics model data")
    val notary = DataHelper.daoStateAndRefFor(inputDao.daoKey, flowLogic.serviceHub).state.notary
    val descriptors = identifyNewDescriptors(inputDao)

    // create dao account
    createDaoAccount(flowLogic, inputDao, notary)

    // create token type if issuer
    descriptors.filter { it.issuerName == flowLogic.ourIdentity.name }.forEach {
      log.info("creating token type ${it.symbol}")
      flowLogic.subFlow(CreateTokenTypeFlow(it.symbol, it.exponent, notary))
    }
  }

  @Suspendable
  private fun createDaoAccount(flowLogic: FlowLogic<*>, inputDao: DaoState, notary: Party) {
    val accountAddress = AccountAddress(inputDao.name, flowLogic.ourIdentity.name)
    if (!Account.exists(flowLogic.serviceHub, accountAddress)) {
      log.info("${inputDao.name} does not exist - creating and tagging")
      flowLogic.subFlow(CreateAccountFlow(listOf(CreateAccountFlow.Request(inputDao.name)), notary))
      log.info("${inputDao.name} account created, now tagging with $DAO_TAG!")
      flowLogic.subFlow(SetAccountTagFlow(inputDao.name, DAO_TAG, notary))
    }
  }

  @Suspendable
  private fun identifyNewDescriptors(previousDaoVersion: DaoState): List<TokenType.Descriptor> {
    return if (previousDaoVersion.containsModelData(EconomicsModelData::class)) {
      val previousEmd = previousDaoVersion.get(EconomicsModelData::class) as EconomicsModelData
      issuableTokens.minus(previousEmd.issuableTokens)
    } else {
      issuableTokens
    }
  }

  @Suspendable
  override fun handleEvent(event: ModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    when (event) {
      is NewMemberModelDataEvent -> addAccountIfWeAreNewMember(event, daoState, flowLogic, notary)
    }
  }

  @Suspendable
  private fun addAccountIfWeAreNewMember(event: NewMemberModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    if (event.newMember == flowLogic.ourIdentity) {
      // TODO - fix this
//      createDaoAccount(flowLogic, daoState, notary)
      log.info("also got here...")
    }
  }

}

