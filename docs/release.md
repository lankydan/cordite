<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Release Process

Below is the release process we have in place for Cordite and some considerations regarding the process we can work through.
It is a work in process and we expect to improve this over time. This is the current standing of the progress; which we can alter as we experience each release.


## Releasing a new version of Cordite

### Prerequisites
1. Check what was the [last tag number](https://gitlab.com/cordite/cordite/tags) of Cordite was
0. pull the lastest version of master on your local repo `git pull origin master`
0. Check that the [last master pipeline] has passed
0. Check what [commits](https://gitlab.com/cordite/cordite/commits/master) there have been since the [last tag](https://gitlab.com/cordite/cordite/tags). This will be used in the release notes following [this format](https://www.prodpad.com/blog/writing-release-notes/)
### Branching off master and updating cordite version

1. Check that the latest [master build is passing](https://gitlab.com/cordite/cordite/pipelines)
0. Pull latest version of master - `git pull origin master`
0. Create a new branch of the version you are cutting (ie branch v0.0.1) - `git checkout -b v0.0.1`
0. On this branch, change the cordite version to the version you are cutting (without "-SNAPSHOT")
```
# Replace the {version} with the version of tag you are releasing
$ sed -i.bak 's/\(version=\).*/\1{version}/g' gradle.properties
```
5. Push this change in with the following commit message - `Cutting v0.0.1 branch and updating version number`


### Creating the tag
0. A branch pipeline will run with the cordite version change. Make sure this [pipeline passes](https://gitlab.com/cordite/cordite/pipelines)
0. Click on the `commit id` of the branch pipline
0. On the top right, click on `Options` and then `Tag`
0. Name the tag as the version you are creating, add a message (e.g. `Tagging new version of Cordite - {version}`) and put the list of commits you gathered earlier

### Post creating tag

1. A tag pipeline will run after the tag is created. Make sure this [pipeline passes](https://gitlab.com/cordite/cordite/pipelines)
0. Check the jars have been published to [maven central](https://repo1.maven.org/maven2/io/cordite/). There is a folder for each of the cordapps. Check each folder to make sure it has a jar with the version just released.
0. Update the cordaledger/cordite channel with the new release and some info about the release. 
0. Update the bluebank.io/cordite channel with the new release and some info about the release.
0. Tweet from `@we_are_cordite` account of new release of cordite
0. Check [docker hub](https://hub.docker.com/r/cordite/cordite/), under `tags` your new version will be part of the list.
0. Update the [docker hub](https://hub.docker.com/r/cordite/cordite/) description with the addition of the new tag under `Supported tags and respective Dockerfile links`. Login credentials in keybase.

### Maven central steps

Essentially:
1. Log into https://oss.sonatype.org using the password in keybase for `community@cordite.io`
2. Click open the “staged repositories” and search for `iocordite`. There should be only one that matches. 
3. Check the contents looks right
4. Press `Close`
5. Hit the `Refresh` button in the Nexus UI and select the “Actions” tab. 
6. Keep clicking “Refresh” until all the actions are done and the the repository has status “Closed”
7. Click “Release” to release to Maven central
8. Check Maven central repo

## Release versioning - SEMVER

For the moment, we are using SEMVER loosely and will become more strict with how we are versioning from release v1.0.0 onwards. For now, we want to understand what out bounderies are and what services we are exposing that our clients will interact with. Over time we will better the versioning we use for cordite. 

Each release will be checked to see what changes are made a which version to release (major, minor, patch) to determine the version number. See here for more info -- https://semver.org/. 

We will be using the release process following steps from Trunk Based development. In summary, a few hours prior to release a new branch is created that acts as the release branch. Given the time before release, there is time to make sure the release branch is stable and prepped to go live. On release day, a tag is created from the release branch.

## Milestone / Issues Management

1. On each release a new milestone is created for the next release
2. All open issues are moved to the new milestone
3. Current release milestone is closed if all issues are closed
4. Any open issues are moved or milestone remains open until issue is closed.

Issues relating to patches **should not** be placed under any milestone.

## Patching

Changes that are critical to a current release are to be done on the release branch. If the change is not specific to that release, the dev making said change must cherry-pick the commit into master. Steps for cherry-picking are below.

## Cherry-Picking

1. Through the gitlab UI, copy the commit SHA you wish to cherry pick
2. Run the following command using your `<SHA>`

```
$ git cherry-pick -n <SHA>
```

You can now commit that change onto your local branch.


## Additional Notes / Considerations

Some topics that need to be considered as we iterate the release process.

- Managing the gradle.properties files over release versions/branches and master - need to discuss and come to consensus on how to approach this aspect of code. Currently on each release we are changing the version of cordite in the file, and then having to manually update it on master to the next release version. Need to find a way to remove this as a step of the release process
- Merging commits/patches between release branch and master - a better way to merge patches/ bug fixes between release branch and master
- Automating the release process to make it more consistent and refined
