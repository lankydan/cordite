Introduction
============

.. toctree::
   :hidden:
   
   overview/history
   overview/architecture
   overview/roadmap



One of the key unsolved challenges is how to support a decentralised
business model. When building decentralised applications, we believe
that there is no point replacing one centralised business model with
another. 2018 will be the year that many in the blockchain domain wake
up to the fact that there is little point in adopting DLT and
distributing their technology if they are not going to distribute the
business model as well.

Cordite provides de-centralised governance and economic services to mutually distrusting corporations wishing to solve distributed challenges in their industries.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cordite provides decentralised economic and governance services
including:

-  decentralised stores and transfers of value allowing new financial
   instruments to be created inside the existing regulatory framework.
   eg. tokens, crypto-coins, digital cash, virtual currency, distributed
   fees, micro-billing
-  decentralised forms of governance allowing new digital autonomous
   organisations to be created using existing legal entities eg. digital
   mutual societies or digital joint stock companies
-  decentralised consensus in order to remove the need for a central
   operator, owner or authority. Allowing Cordite to be more resilient,
   cheaper, agile and private than incumbent market infrastructure

Built on Corda
~~~~~~~~~~~~~~

Distributed Ledger Technology (DLT) (‘blockchain’) is a disruptive
technology that could enable institutions to reduce costs, improve
product offerings and increase speed. There has been an explosion of
activity - with institutions actively researching, testing and investing
in this technology. Financial institutions have remained at the
forefront of this development, and have established a strong insight
into the underlying technology, the market landscape and the potential
value applications for their businesses.

Cordite is built on `Corda <http://corda.net>`__, a finance grade
distributed ledger technology, meeting the highest standards of the
banking industry, yet it is applicable to any commercial scenario. The
outcome of over two years of intense research and development by over 80
of the world’s largest financial institutions.

Cordite is open source, regulatory friendly, enterprise ready and
finance grade.
